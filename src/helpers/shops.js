export const SHOPS = {
  AUCHAN: 'Auchan',
  AUCHAN_MINI: 'Auchan-mini',
  JUMBO: 'Jumbo',
  KAUFLAND: 'Kaufland',
  LIDL: 'Lidl',
  CARREFOUR: 'Carrefour'
};
